# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
	"name": "NoSa Dynamic Template Report Example",
	'category': 'Extra Tools',
	"summary": "",
	"version": "1.0",
	"author": "NS Thang",
	"support": "ducthangict.dhtn@gmail.com",
	"license": "AGPL-3",
	"website": "https://voducthang.com",
	"depends": [
		'base',
		'sale'
	],
	'data': [
		'reports/sale_order_invoice.xml'
	],
	'images': ['static/description/bg.png'],
	"installable": True,
	"application": True,
}
